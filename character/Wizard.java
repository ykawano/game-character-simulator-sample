package character;

// 魔法使いのクラス
public class Wizard extends GameCharacter {

	// パラメータ成長値の設定
	static final double hp_up = 2.5, mp_up = 3.7,
			power_up = 0.7, speed_up = 1.4, guard_up = 0.9,
			intelligent_up = 2.0, luck_up = 1.8;
	static final double range = 2.5;

	public Wizard(String name, Sex sex, Job job, int hp, int mp,
			int power, int speed, int guard, int intelligent,
			int luck) {
		super(name, sex, job, hp, mp, power, speed, guard,
				intelligent, luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	// レベル指定
	public Wizard(String name, Sex sex, Job job, int hp, int mp,
			int power, int speed, int guard, int intelligent,
			int luck, int level) {
		super(name, sex, job, hp, mp, power, speed, guard,
				intelligent, luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
		// TODO 自動生成されたコンストラクター・スタブ
		this.level = level;
	}

	public Wizard(String name, Sex sex, Job job) {
		super(name, sex, job, 12, 8,6, 9, 6, 10, 10,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
	}
}
