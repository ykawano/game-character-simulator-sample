package character;

//職業毎のクラス（勇者, 戦士, 魔法使いなど）
public class Job {
	private String name; // 職業名
	private int number;  // 職業ID
	public int hp, mp,   // 各種パラメータ：HP, MP, 力, 早, 身, 賢, 運
			power, speed, guard, intelligent, luck;

	public Job(String name, int number, int hp, int mp,
			int power, int speed, int guard, int intelligent, int luck) {
		this.name = name;
		this.number = number;
		this.hp = hp; this.mp = mp;
		this.power = power; this.speed = speed; this.guard = guard;
		this.intelligent = intelligent; this.luck = luck;
	}

	public Job(String name, int number) {
		this.name = name;
		this.number = number;
		this.hp = 10; this.mp = 0;
		this.power = 7; this.speed = 5; this.guard = 7;
		this.intelligent = 5; this.luck = 5;
	}

	public String JobName() {
		return name;
	}
	public int JobNumber() {
		return number;
	}

}
