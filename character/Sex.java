package character;

// 性別のクラス
public class Sex {
	private final String name;
	private final int number;

	public Sex(String name, int number) {
		this.name = name;
		this.number = number;
	}

	// ゲッタ
	public String GenderName() {
		return name;
	}
	public int GenderNumber() {
		return number;
	}

}
