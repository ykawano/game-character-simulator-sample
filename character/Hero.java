package character;

// 勇者のクラス
public class Hero extends GameCharacter {

	// パラメータ成長値の設定
	static final double hp_up = 3.5, mp_up = 2.2,
			power_up = 2.0, speed_up = 1.2, guard_up = 1.4,
			intelligent_up = 1.3, luck_up = 1.3;
	static final double range = 2.8;

	public Hero(String name, Sex sex, Job job, int hp, int mp,
			int power, int speed, int guard, int intelligent,
			int luck) {
		super(name, sex, job, hp, mp, power, speed, guard,
				intelligent, luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	// レベル指定
	public Hero(String name, Sex sex, Job job, int hp, int mp,
			int power, int speed, int guard, int intelligent,
			int luck, int level) {
		super(name, sex, job, hp, mp, power, speed, guard,
				intelligent, luck,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
		// TODO 自動生成されたコンストラクター・スタブ
		this.level = level;
	}

	public Hero(String name, Sex sex, Job job) {
		super(name, sex, job,18, 5,12, 8, 10, 8, 6,
				hp_up, mp_up, power_up, speed_up, guard_up, intelligent_up, luck_up, range);
	}


}
