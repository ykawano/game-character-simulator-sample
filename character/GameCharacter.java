package character;

// ゲームキャラクターの抽象クラス
public abstract class GameCharacter {
	String name; Sex sex;				// 名前、性別
	int level; Job job;					// レベル, 職業
	protected int MaxHp, MaxMP;			// 最大HP、最大MP
	protected int hp, mp;				// HP、MP
	protected int power, speed, guard;	// ちから、すばやさ、みのまもり
	protected int intelligent, luck;	// かしこさ、うんのよさ

	protected double hp_up, mp_up, 		// パラメータ上昇値
	power_up, speed_up, guard_up,
	intelligent_up, luck_up;
	protected double range; 				// 成長幅


	// コンストラクタの基本
	public GameCharacter (String name, Sex sex, Job job,
			int hp, int mp,
			int power, int speed, int guard,
			int intelligent, int luck) {

			// コンストラクタとしての処理を書くこと

			this.name = name; this.sex = sex; this.job = job;
			this.MaxHp = hp; this.MaxMP = mp; this.hp = hp; this.mp = mp;
			this.power = power; this.speed = speed; this.guard = guard;
			this.intelligent = intelligent;
			this.luck = luck;
			this.level = 1;

	}

	// パラメータ上昇値を指定したコンストラクタ
	public GameCharacter (String name, Sex sex, Job job,
			int hp, int mp,
			int power, int speed, int guard,
			int intelligent, int luck,
			double hp_up, double mp_up,
			double power_up, double speed_up, double guard_up,
			double intelligent_up, double luck_up, double range) {

			// コンストラクタとしての処理を書くこと
			// パラメータ上昇値設定


			// 初期パラメータ設定
			this.name = name; this.sex = sex; this.job = job;
			this.MaxHp = hp; this.MaxMP = mp; this.hp = hp; this.mp = mp;
			this.power = power; this.speed = speed; this.guard = guard;
			this.intelligent = intelligent;
			this.luck = luck;
			this.level = 1;

			// パラメータ上昇値設定
			this.hp_up = hp_up; this.mp_up = mp_up;
			this.power_up = power_up; this.speed_up = speed_up; this.guard_up = guard_up;
			this.intelligent_up = intelligent_up; this.luck_up = luck_up;
			this.range = range;


	}
	//public abstract void fight (GameCharacter c); // 攻撃
	//public abstract void damage (int damage);  	// ダメージ

	// レベルアップのメソッド
	public void levelUp () {
		// パラメータ上昇の処理を書くこと
		// パラメータ上昇
		this.MaxHp += (int)(Math.random()*range+hp_up);
		this.MaxMP += (int)(Math.random()*range+mp_up);
		int power_plus = (int)(Math.random()*range+power_up);
		this.power += power_plus;
		this.speed += (int)(Math.random()*range+speed_up);
		int guard_plus = (int)(Math.random()*range+guard_up);
		this.guard += guard_plus;
		this.intelligent += (int)(Math.random()*range+intelligent_up);
		this.luck += (int)(Math.random()*range+luck_up);
		this.level++;

		// HP, MP全回復
		this.hp = this.MaxHp; this.mp = this.MaxMP;

	}

	// ステータス表示
	public void displayStatus() {		// ステータス表示
		System.out.println("------------------------");
		System.out.println(this.name+"\n"+"せいべつ： "+this.sex.GenderName()+"\n"
				+this.job.JobName()+", レベル: "+this.level+"\n"
				+"HP: "+this.hp+", MP: "+this.mp+"\n");
		System.out.println("ちから: "+this.power+"\n"+"すばやさ: "+this.speed+"\n"
				+"みのまもり: "+this.guard+"\n"+"かしこさ: "+this.intelligent+"\n"
				+"うんのよさ: "+this.luck+"\n"
				+"さいだいHP: "+this.MaxHp+"\n"+"さいだいMP: "+this.MaxMP);
		System.out.println("------------------------\n");
	}

}