package character;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// キャラクター作成処理のクラス
public class CharacterMaking {
	final static int MAX_JOBS = 3; // 職業の数

	// キャラクター作成時のパラメータ設定
	private static GameCharacter RandomParameter(String name, Sex sex, Job job) {
		GameCharacter chara=null;
		switch (job.JobNumber()) {
		case 1:
			// TODO：勇者クラスのオブジェクト作成
			chara = new Hero(name, sex, job, job.hp, job.mp,
					job.power, job.speed, job.guard,
					job.intelligent, job.luck);
			break;
		case 2:
			// TODO：戦士クラスのオブジェクト作成
			chara = new Warrior(name, sex, job, job.hp, job.mp,
					job.power, job.speed, job.guard,
					job.intelligent, job.luck);
			break;
		case 3:
			// TODO：魔法使いクラスのオブジェクト作成
			chara = new Wizard(name, sex, job, job.hp, job.mp,
					job.power, job.speed, job.guard,
					job.intelligent, job.luck);
			break;
		}
		return chara;
	}

	// キャラクター作成メソッド
	public static GameCharacter making () {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String name="";
		String buf="";

		// キャラクター情報
		GameCharacter chara=null;
		Sex sex;
		Job job=null;

		Job[] jobs = new Job[MAX_JOBS]; // Job型オブジェクトの配列を宣言
		jobs[0] = new Job("ゆうしゃ", 1,18, 5,12, 8, 10, 8, 6);
		jobs[1] = new Job("せんし", 2,	20, 0,14, 5, 10, 4, 5);
		jobs[2] = new Job("まほうつかい", 3,12, 8,6, 9, 6, 10, 10);


	    System.out.println("なまえをいれてください。");
	    try {
			name = new String(in.readLine());
		} catch (IOException e) {
			// 自動生成された catch ブロック
			e.printStackTrace();
		}

	    do {
		    System.out.println("つぎに、せいべつをおしえてください。\n男:1, 女:2");
		    try {
				buf = new String(in.readLine());
			} catch (IOException e) {
				// 自動生成された catch ブロック
				e.printStackTrace();
			}
		    int num = Integer.parseInt(buf);
		    if (num == 1 ) {
		    	sex = new Sex("おとこ", 1);
		    	break;
		    }
		    else if (num == 2) {
		    	sex = new Sex("おんな", 2);
		    	break;
		    }
	    } while (true);

	    do {
		    System.out.println("職業を選択してください。");
		    for (int i=0; i<MAX_JOBS; i++) {
		    	System.out.print(jobs[i].JobNumber()+":"+jobs[i].JobName()+"  ");
		    }
		    System.out.println("\n");
		    try {
				buf = new String(in.readLine());
			} catch (IOException e) {
				// 自動生成された catch ブロック
				e.printStackTrace();
			}
		    // 職業の入力
		    int job_number = Integer.parseInt(buf);

		    // TODO：職業毎のパラメータ設定処理
		    for (int i=0; i<MAX_JOBS; i++) {
		    	if (jobs[i].JobNumber() == job_number) {
		    		job=jobs[i];
		    		chara = CharacterMaking.RandomParameter(name, sex, job);
					//chara.displayStatus();
		    	}
		    }

	    } while (job == null); // 正しい職業選択ができるまで繰り返す
	    return chara;
	}

	// レベルアップシミュレーションのメソッド
	public static void levelSimulation (GameCharacter chara) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String buf="";
		int level=1;
		do {
			System.out.println("何レベルまで成長させますか？");
			try {
				buf = new String(in.readLine());
				level = Integer.parseInt(buf);
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			if (level >= chara.level && level <= 99) break;
		} while(true);

		// TODO：レベルアップシミュレーションの処理
		for (int i=chara.level; i<level; i++) {
			chara.levelUp();
		}

		return;
	}
}
