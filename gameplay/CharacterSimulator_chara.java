package gameplay;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import character.CharacterMaking;
import character.GameCharacter;
import character.Hero;
import character.Job;
import character.Sex;
import character.Warrior;
import character.Wizard;

public class CharacterSimulator_chara{
	// Topメニューの表示
	static int showMenu() throws IOException  {
		int menu_num = 0;

		System.out.println("メニューから番号を入力してください。");
		System.out.println("1. キャラクター表示\n2. キャラクター作成\n3. レベルアップシミュレーション\n"
				+ "4. セーブ\n5. ロード\n6. 終了");
		BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));

		try {
			menu_num = Integer.parseInt(buf.readLine());
		} catch (Exception e) {
			System.out.println("数値で入力してください。");

		}
		System.out.println(menu_num + "が入力されました.");

        return menu_num;

	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		GameCharacter chara = null;

		// メニュー表示
		int menu_number = 0;
		do {
			menu_number = showMenu();
			switch (menu_number) {
			case 1:
				// キャラクター表示
				if (chara != null) {
					chara.displayStatus();
				}
				else {
					System.out.println("キャラクターが作成されておりません。\n2を選んで、キャラクターを作成してください。\n");
				}

				break;
			case 2:
				// キャラクター作成
				chara = CharacterMaking.making();
				chara.displayStatus();

				break;
			case 3:
				// キャラクター成長シミュレーション
				if (chara != null) {
					chara.displayStatus();
					CharacterMaking.levelSimulation(chara);
					chara.displayStatus();
				}
				else {
					System.out.println("キャラクターが作成されておりません。\n2を選んで、キャラクターを作成してください。\n");
				}

				break;
			case 4:
				// TODO：ゲームデータのセーブ（JSONファイルへの保存）
				System.out.println("ゲームデータをセーブしました。\n");
				break;
			case 5:
				// TODO：ゲームデータのロード（JSONファイルからキャラクターのオブジェクトに展開）
				System.out.println("ゲームデータをロードします。\n");

				try {
					// ObjectMapperでJSONファイルの木構造を作成
					ObjectMapper mapper = new ObjectMapper();
					JsonNode root = mapper.readTree(new File("src/character.json"));

					// セーブデータから値の取得
					String name = root.get("name").asText();
					int sex_number = root.get("sex").asInt();
					int job_number = root.get("job").asInt();
					int level = root.get("level").asInt();
					int hp = root.get("hp").asInt();
					int mp = root.get("mp").asInt();
					int power = root.get("power").asInt();
					int speed = root.get("speed").asInt();
					int guard = root.get("guard").asInt();
					int intelligent = root.get("intelligent").asInt();
					int luck = root.get("luck").asInt();

					// 性別の設定
					Sex sex=null;
					if (sex_number == 1 ) {
						sex = new Sex("おとこ", 1);
					}
					else if (sex_number == 2) {
						sex = new Sex("おんな", 2);
					}
					else {
						System.out.println("性別の値が不正です。\n");
						break;
					}

					// 職業の設定
					Job job=null;
					if (job_number == 1 ) {
						job = new Job("ゆうしゃ", 1);
						chara = new Hero(name,sex,job,hp,mp,
								power, speed, guard, intelligent, luck, level);
					}
					else if (job_number == 2) {
						job = new Job("せんし", 2);
						chara = new Warrior(name,sex,job,hp,mp,
								power, speed, guard, intelligent, luck, level);
					}
					else if (job_number == 3) {
						job = new Job("まほうつかい", 3);
						chara = new Wizard(name,sex,job,hp,mp,
								power, speed, guard, intelligent, luck, level);
					}
					else {
						System.out.println("職業の値が不正です。\n");
						break;
					}

					// ステータス表示
					chara.displayStatus();
				}
				catch (FileNotFoundException fe) {
					fe.printStackTrace();
				} catch (Exception e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}

				System.out.println("ゲームデータをロードしました。\n");
				break;
			case 6:
				System.out.println("RPGキャラクターシミュレーションを終了します。\n");
				break;
			default:
				System.out.println("有効な番号を入力してください。\n");
				break;
			}

		} while (menu_number != 6);
	}
}